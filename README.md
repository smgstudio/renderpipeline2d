# README #

### What is this repository for? ###

* Copy of the Universal Render Pipeline created by Unity. This version adds support for 2D renderer features.
* Version 8.2.0

### How do I get set up? ###

* Modify your manifest to use this library: 
	"com.unity.render-pipelines.universal": "https://git@bitbucket.org/smgstudio/renderpipeline2d.git?path=/com.unity.render-pipelines.universal"